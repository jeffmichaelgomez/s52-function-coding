function countLetter(letter, sentence) {
    let result = 0;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    if(letter.length == 1){
        for(let i = 0; i < sentence.length; i++){
            if(sentence.toLowerCase().charAt(i) == letter.toLowerCase()){
               result++;
            } 
        }
    }
    else{
        result = undefined;

    }
    return result;
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    for(let x = 0; x <text.length; x++ ){
        for(let i = x+1; i < text.length; i++){
            if(text.toLowerCase().charAt(i) == text.toLowerCase().charAt(x)){
                  return false
            }
        }
    }
    return true  
}


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if(age < 13){
        return undefined;
    }
    else if((age >= 13)&&(age <= 21)){
        price = (price*.8).toFixed(2)
    }
    else if((age >= 22)&&(age <= 64)){
       price = price.toFixed(2)
    }
    else{
        price = (price*.8).toFixed(2)
    }

    return price.toString();

}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' } ]
    let hotCategories = [];
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    for(let i = 0; i<items.length; i++){
        if(items[i].stocks == 0){
            if(hotCategories.includes(items[i].category) === false){
                hotCategories.push(items[i].category);
            }
        }
    }
    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
   candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
   candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
   let flyingVoters = [];

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    for(let x = 0; x < candidateA.length; x++ ){
        for(let i = 0; i < candidateB.length; i++){
            if(candidateB[i] == candidateA[x]){
                flyingVoters.push(candidateB[i]);
                break;
            }
        }
    }
    return flyingVoters;
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};